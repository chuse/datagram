"""
	Copyright 2014 Jose Maria Miotto (josemiotto+datagram@gmail.com)

	This file is part of datagram.

	datagram is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	datagram is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with datagram.  If not, see <http://www.gnu.org/licenses/>.
"""

from Distribution import distributions
import numpy as np
import mpmath as mp
import scipy.optimize
import bisect

"""
Auxiliary functions for the Generalized Pareto Distribution fitting function
implemented from Grimshaw, Technometrics 35 (2) 1993, pags. 185-191.
"""

def h(data,theta):
	x = np.array([ 1.0-theta*z[0] for z in data ])
	y = np.array([ z[1] for z in data ])
	N    = sum( y )
	wlog = sum( y*np.log(x) )/N
	w1m  = sum( y/x )/N
	hf   = ( 1.0 + wlog )*w1m - 1.0
	return hf

def dh(x,y,theta):
	x1   = 1.0-theta*x
	N    = sum( y )
	wlog = sum( y*np.log(x1) )/N
	w1m  = sum( y/x1 )/N
	w2m  = sum( y/(x1**2.0) )/N
	hf   = ( 1.0 + wlog )*w1m - 1.0
	hf1  = ( w2m - w1m**2.0 - wlog*(w1m-w2m) )/theta
	d = hf/hf1
	while theta-d>0.0:
		d = d/2.0
	return d

def h2lim(m1,m2):
	return m2-2.0*m1**2.0



"""
	General functions to fit, there are many options.
"""

def fit_gen(hist, fit_type, threshold=None):
	
	t = threshold
	if t is not None:
		idx = bisect.bisect_left(hist.x, t)
	else:
		idx = 0
	X = hist.x[idx:]
	Y = hist.hist[idx:]
	N = np.sum(Y)
	logpdf = distributions[fit_type]['logpdf']
	if 'norm' in distributions[fit_type].keys():
		norm = distributions[fit_type]['norm']
	else:
		norm = 0.0
	
	def like(p):
		return np.sum(Y*(logpdf(X,p)))-N*norm(X,p)

	def draw(*bounds):
		return [np.random.uniform(*bound) for bound in bounds]

	bounds = ((-4.0,-0.01),(1.0,1000.0))
	p0 = draw(bounds)
	p1 = p0[:]
	l = like(p1)
	it, nits = 0, 10
	while it<nits:
		p0 = draw(bounds)
		try:
			res = scipy.optimize.minimize(like, p0, method='L-BFGS-B', bounds=bounds)
		except:
			pass
		else:
			l0 = like(res.x)
			if l0<l:
				l = l0
				p1 = res.x[:]
			it += 1

	if 'threshold' in distributions[fit_type]['parameters']:
		parameters = [p1[0],p1[1],threshold]
	else:
		parameters = p1[:]
	hist.fits[fit_type] = { x:parameters[i] for i,x in enumerate(distributions[fit_type]['parameters']) }

def fit(hist,fit_type,**kwargs):
	"""
	Method to fit a distribution, given by 'fit_type'; kwargs are the necessary parameters for the given distribution.
	"""
	try:
		hist.fits
	except:
		hist.fits = {}

	if (fit_type == 'Pareto') or (fit_type=='DiscretePareto'):
		threshold = kwargs['threshold']
		idx = next(i for i,z in enumerate(hist.x) if z>threshold)
		x = hist.x[idx:]-threshold
		y = hist.hist[idx:]
		N = np.sum(y)
		m1 = np.sum( y*x )/N
		m2 = np.sum( y*x**2.0 )/N
		eps = 1e-6/m1
		xmin = min(x)
		xmax = max(x)
		theta_L = 2.0*(xmin-m1)/xmin**2.0
		theta_U = 1.0/xmax-eps
		h2 = h2lim(m1,m2)
		if h2>0.0:
			theta = theta_L
			d = dh(x,y,theta)
			i = 0
			while (abs(d)>eps):
				i += 1
				d = dh(x,y,theta)
				theta -= d
		else:
			print 'h2<0'
			#exit()
			raise
# 			pass
		k = -np.sum(y*np.log(1.0-theta*x))/N
		a = k/theta
		hist.fits[fit_type] = { 'k':k, 'a':a, 'threshold':threshold }

	else:
		print 'The requested Fit Type is currently not supported.'

#k,a,threshold

def fit_cont(hist, fit_type, threshold=1.0):
	try:
		hist.fits
	except:
		hist.fits = {}
	for i,x in enumerate(hist.x):
		if x>threshold:
			idx=i
			break
	X = hist.x[idx:]
	Y = hist.hist[idx:]

	alpha = 1.0+float(hist.N)/(sum(Y*np.log(X/float(X[0]))))
	k = 1.0/(1.0-alpha)
	parameters = [k,-k/threshold,threshold]

	hist.fits[fit_type] = {x:parameters[i] for i,x in enumerate(distributions[fit_type]['parameters'])}

def fit_disc(hist, fit_type, threshold=1.0):
	try:
		hist.fits
	except:
		hist.fits = {}

	idx = bisect.bisect_left(hist.x,threshold)
	X = hist.x[idx:]
	Y = hist.hist[idx:]
	N = np.sum(Y)
	r = len(X)

	def like(hist,p):
		try:
			norm = float(((-p[1]/p[0])**(1.0-1.0/p[0]))*mp.zeta(1.0-1.0/p[0],-p[1]/p[0]))
		except ValueError:
			print p, 'ValueError'
			exit()
		z = N*np.log(norm)-sum( Y[i]*np.log(1.0-(X[i]-p[2])*p[0]/p[1]) for i in xrange(r) )*(1.0/p[0]-1.0)
		return z

	nits = 50
	eps = 0.01
	nste1,nste2 = 1000,1000
	p0 = [-0.5,2.0*threshold,threshold]

	l = like(hist,p0)
	l0 = l
	lm = l

	for k in xrange(nits):

		p1 = p0[:]
		pm0 = p0[0]
		for h in xrange(nste1/10,nste1):
			pm = -float(h)/float(nste1)
			p1[0] = pm
			try:
				l = like(hist,p1)
			except:
				print p1
				exit()
			if l<l0:
				l0 = l
				pm0 = pm
		if p0[0] != pm0:
			p0[0] = pm0

		p1 = p0[:]
		pm0 = p0[1]
		for h in xrange(3,nste2):
			pm = float(h)*50.0/float(nste2)
			p1[1] = pm
			l = like(hist,p1)
			if l<l0:
				l0 = l
				pm0 = pm
		if p0[1] != pm0:
			p0[1] = pm0

		if abs(lm-l0)<eps:
			break
		else:
			lm = l0

	parameters = p0
	hist.fits[fit_type] = {x:parameters[i] for i,x in enumerate(distributions[fit_type]['parameters'])}



def fit_disc2(hist, fit_type, k=None, a=None, threshold=1.0):
	try:
		hist.fits
	except:
		hist.fits = {}
	for i,x in enumerate(hist.x):
		if x>threshold:
			idx=i
			break
	X = hist.x[idx:]
	Y = hist.hist[idx:]
	N = np.sum(Y)
	r = len(X)

	def like(hist,p):
		try:
			norm = float(((-p[1]/p[0])**(1.0-1.0/p[0]))*mp.zeta(1.0-1.0/p[0],-p[1]/p[0]))
		except ValueError:
			print p, 'ValueError'
			exit()
		z = N*np.log(norm)-sum( Y[i]*np.log(1.0-(X[i]-p[2])*p[0]/p[1]) for i in xrange(r) )*(1.0/p[0]-1.0)
		return z

	nits = 50
	eps = 0.01
	n1,n2 = 1000,1000
	
	p0 = [-0.6,3.53,1.0]
	l = like(hist,p0)
	
	dk = [-2.0,-0.0]
	da = [threshold,10*threshold]

	variables = [0,1]
	delta = [dk,da]
	n = [n1,n2]
	p0 = [-0.5,2.0*threshold,threshold]
	skip = [False,False]
	if k is not None:
		p0[0] = k
		skip[0] = True
	if a is not None:
		p0[1] = a
		skip[1] = True

	l = like(hist,p0)
	l0 = l
	lm = l

	def search(hist,p0,values,j):
		l0 = like(hist,p0)
		p1 = p0[:]
		for pm in values:
			p1[j] = pm
			l = like(hist,p1)
			d = l-l0
			if d<0:
				l0 = l
				p0[j] = pm
			if d>100:
				break
		return p0, l0

	for k in xrange(nits):
		for m in variables:
			if skip[m] is True:
				continue
			for h in [1,0]:
				values = [ p0[m]+(delta[m][h]-p0[m])*float(s)/float(n[m]) for s in xrange(1,n[m]-1) ]
				p0,l0 = search(hist,p0,values,m)

		if abs(lm-l0)<eps:
			break
		else:
			lm = l0

	parameters = p0
	hist.fits[fit_type] = {x:parameters[i] for i,x in enumerate(distributions[fit_type]['parameters'])}




def fit_disc3(hist, fit_type, threshold=1.0):

	t = threshold
	idx = bisect.bisect_left(hist.x, t)
	X = hist.x[idx:]
	Y = hist.hist[idx:]
	N = np.sum(Y)

	def like(p):
		try:
			z = mp.zeta(1.0-1.0/p[0],-p[1]/p[0])
			norm = (-p[1]/p[0])**(1.0-1.0/p[0])*float(z)
		except ValueError:
			print p, 'ValueError'
			exit()		
		z = N*np.log(norm)
		z += (1.0-1.0/p[0])*np.sum( Y*np.log(1.0-(X-threshold)*p[0]/p[1]))
		return z

	p0 = [-0.5,35.0]
	nits = 3
	p1 = p0[:]
	l = like(p1)
	bound1 = (-4.0,-0.01)
	bound2 = (1.0,1000.0)
	for i in xrange(nits):
		p0 = [np.random.uniform(*bound1),np.random.uniform(*bound2)]
		res = scipy.optimize.minimize(like, p0, method='L-BFGS-B', bounds=(bound1,bound2))
		l0 = like(res.x)
		if l0<l:
			l = l0
			p1 = res.x[:]

	parameters = [p1[0],p1[1],threshold]
	
	hist.fits[fit_type] = { x:parameters[i] for i,x in enumerate(distributions[fit_type]['parameters']) }




"""
	Functions for Statistical Assesment of the fits.
"""


#def KS_statistics(hist,fit_type):
	#"""
	#Compute the Kolmogorov Smirnov statistic for the distribution given by fit_type. Requires a previous fit.
	#"""
	#p = hist.fits[fit_type]
	#idx = next(i for i,z in enumerate(hist.x) if z>=p['threshold'])
	#x = hist.x[idx:]-p['threshold']
	#y = hist.ccdf[idx:] # CCDF
	#if fit_type == 'pareto':
		#fit = y[0]*(1.0-x*p['k']/p['a'])**(1.0/p['k']) # CCDF
	#else:
		#print 'The requested Fit Type is currently not supported.'
	#KS = abs(fit-y)
	#hist.fits[fit_type]['KS_D'] = np.amax(KS)

#import time

def KS_statistics(hist,dist):
	"""
	Compute the Kolmogorov Smirnov statistic between data in a histogram and a
	given distribution.
	"""
	p = dist.parameters
	t = p['threshold']
	idx = bisect.bisect_left(hist.x,t)
	x = hist.x[idx:]
	y = hist.ccdf[idx:]# CCDF
	if dist.__class__.__name__=='DiscreteParetoDistribution':
		##z = np.array([dist.ccdf[int(x1)] for x1 in x])
		
		#t0 = time.clock()
		#for i in range(100):
			#z = np.array([dist.ccdfA[int(x1)-t-1] for x1 in x])
		#print 'method 1', time.clock()-t0
		
		#z = np.array([dist.ccdf(x1-t) for x1 in x])
		
		x0 = len(dist.ccdfA)-t-1
		if x[-1]>x0:
			i = 1
			while x[-i]>x0:
				i += 1
				i0 = i
			z = [dist.ccdfA[int(x1)-t-1] for x1 in x[:-i0]]
			for x1 in x[-i0:]:
				z.append(dist.ccdf(x1-t))
		else:
			z = [dist.ccdfA[int(x1)-t-1] for x1 in x]
		z = np.array(z)
	else:
		z = dist.ccdf(x)
	
	
	y = y/y[0]
	z = z/z[0]
	KS = np.abs(y-z)
	d = np.amax(KS)
	if np.isnan(d)==False:
		idx2 = list(KS).index(d)
		print d,idx2,x[idx2],y[idx2]/y[0],z[idx2]/z[0]
	return d
