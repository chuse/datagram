#__all__ = [ 'Histogram', 'Distribution' ]

#for module in __all__:
    #__import__(module, globals(), locals(), [])

from Histogram import Histogram
from Distribution import *
from fit import fit_gen, fit, fit_cont, fit_disc, fit_disc2, fit_disc3, KS_statistics