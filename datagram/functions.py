"""
    Copyright 2014 Jose Maria Miotto (josemiotto+datagram@gmail.com)

    This file is part of datagram.

    datagram is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    datagram is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with datagram.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import mpmath as mp

distributions = {
'Pareto': { # parameters; k, a, threshold
    'parameters': ['k','a','threshold'],
    'pdf':    lambda x,p: (1.0-(x-p[2])*p[0]/p[1])**(1.0/p[0]-1.0)/p[1] ,
    'cdf':    lambda x,p: 1.0-(1.0-(x-p[2])*p[0]/p[1])**(1.0/p[0]) ,
    'ccdf':   lambda x,p: ( 1.0 - (x-p[2])*p[0]/p[1] )**(1.0/p[0]) ,
    'logpdf': lambda x,p: (1.0/p[0]-1.0)*np.log(1.0-(x-p[2])*p[0]/p[1])-np.log(p[1]),
    'invcdf': lambda x,p: p[2]+p[1]/p[0]*(1.0-(1.0-x)**p[0])
    },
'DiscretePareto':{ # parameters; k, a, threshold
    'parameters': ['k','a','threshold'],
    'pdf':    lambda x,p: (1.0-(x-p[2])*p[0]/p[1])**(1.0/p[0]-1.0)/p[3] ,
    'cdf':    lambda x,p: 1.0,
    'ccdf':   lambda x,p: (-p[1]/p[0])**(1.0-1.0/p[0])*float(mp.zeta(1.0-1.0/p[0],x-p[1]/p[0]))/p[3],
    'logpdf': lambda x,p: (1.0/p[0]-1.0)*np.log(1.0-(x-p[2])*p[0]/p[1])-np.log(p[3]),
    'invcdf': 1.0,
    'norm':   lambda x,p: (-p[1]/p[0])**(1.0-1.0/p[0])*float(mp.zeta(1.0-1.0/p[0],-p[1]/p[0]))

    },
'DiscretePareto2':{ # parameters; k, a, threshold
    'parameters': ['k','a','threshold'],
    'pdf':    lambda x,p: (x)**(1.0/p[0]-1.0)/p[3] ,
    'cdf':    lambda x,p: mp.zeta(1.0/p[0]-1.0,x)/p[3],
    'ccdf':   lambda x,p: 1.0-mp.zeta(1.0/p[0]-1.0,x)/p[3],
    'logpdf': lambda x,p: (1.0/p[0]-1.0)*np.log(x)-np.log(p[3]),
    'invcdf': 1.0,
    'norm':   lambda x,p: float(mp.zeta(1.0-1.0/p[0],p[2]))
    },
'Exponential': { # parameters; l
    'parameters': ['l'],
    'pdf':    lambda x,p: np.exp(-x*p[0])*p[0],
    'cdf':    lambda x,p: 1.0-np.exp(-x*p[0]),
    'ccdf':   lambda x,p: np.exp(-x*p[0]),
    'logpdf': lambda x,p: -x*p[0],
    'invcdf': lambda x,p: -np.log(1.0-x)/p[0]
    }
}