"""
	Copyright 2014 Jose Maria Miotto (josemiotto+datagram@gmail.com)

	This file is part of datagram.

	datagram is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	datagram is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with datagram.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import bisect
from functions import distributions

class Distribution(object):
	"""
	Class Distribution: contains standard functions pdf, cdf, ccdf, logpdf, 
	invcdf, prepare and loglikelihood.
	"""

	def prepare(self, n, t=0):
		"""
		generates arrays dist.cdfA and dist.ccdfA for quick searches in the 
		generation of data from non closed expression of the cdf.
		"""
		try:
			t = int(self.parameters['threshold'])
		except:
			pass
		X = np.array(range(t,n))
		Y = self.pdf(X)
		self.cdfA = np.cumsum(Y)
		self.ccdfA = 1.0-np.cumsum(Y)

	def prior(self):
		"""
		Prior distribution of the likelihood (usually flat).
		"""
		return 1.0

	def loglikelihood(self, hist, t0=None, t1=None):
		"""
		LogLikelihood of all data since threshold t.
		"""
# 		idx0 = next(i for i,z in enumerate(hist.x) if z>t0)
# 		idx1 = next(i for i,z in enumerate(hist.x[::-1]) if z<t1)
		idx0 = bisect.bisect_left(hist.x,t0)
		idx1 = bisect.bisect_left(hist.x,t1)
		x = hist.x[idx0:idx1]
		y = hist.hist[idx0:idx1]
		return np.sum(y*(self.logpdf(x)+np.log(self.prior())))

def dist_factory(type_name):
	"""
	Create a Class for a given type of distribution
	"""
	
	dic = distributions[type_name]
	
	def __init__(self,name,**parameters):
		setattr(self,'name',name)
		setattr(self,'parameters',{})
		setattr(self,'p',[])
		for key, value in parameters.items():
			if key not in dic['parameters']:
				raise TypeError('Arguments %s not valid for %s' %(key, self.__class__.__name__))
			self.parameters[key] = value
		self.p = [ self.parameters[z] for z in dic['parameters'] ]

		if 'norm' in dic.keys():
			self.p.append(dic['norm'](0.0,self.p))

# 		Distribution.__init__(self,name)

	fun_dict = {'__init__':__init__}
	for key in dic.keys():
		if key in ['parameters']:
			continue
		else:
# 			def std_function(self,x):
#	 			return distributions[type_name][fun](x,self.p)
# 			fun_dict[fun] = std_function
			fun_dict[key] = lambda self,x,val=dic[key]: val(x,self.p)	#lambda x,val=val:val(x,self.p)
	newclass = type(type_name+'Distribution', (Distribution,), fun_dict)
	return newclass

for type_name in distributions.keys():
	exec('{0}Distribution = dist_factory("{0}")'.format(type_name))